import React from 'react'
import { MdNavigateNext } from 'react-icons/md'
import { FaTractor } from 'react-icons/fa';
const Home = () => {
    return (
        <div className="">
            <div className="container card" style={{ backgroundColor: "lightgray" }}>
                <div className="row p-2">
                    <nav className="col-sm-2 card p-3">
                        <p className="border-bottom align-items-center pb-2">MARKALAR</p>
                        <ul className="nav flex-column align-items-start border-bottom">
                            <li className="nav-item border-bottom ">
                                <img src="../img/Markalar/MasseyFerguson.jpg" /><a className="nav-link active" style={{ fontSize: "10px", color: "black", float: "right" }} aria-current="page" href="#">Massey Ferguson<span><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom ">
                                <img src="../img/Markalar/NewHolland.jpg" /> <a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">New Holland<span className="ps-3"><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom">
                                <img src="../img/Markalar/Fiat.jpg" /><a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">Fiat<span className="ps-5"><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom">
                                <img src="../img/Markalar/Ford.jpg" /> <a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">Ford<span className="ps-5"><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom">
                                <img src="../img/Markalar/Same.jpg" /> <a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">Same<span className="ps-5"><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom">
                                <img src="../img/Markalar/JohnDeere.jpg" /><a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">John Deere<span className="ps-4"><MdNavigateNext /></span></a>
                            </li>
                            <li className="nav-item border-bottom">
                                <img src="../img/Markalar/Tümosan.jpg" /> <a className="nav-link" style={{ fontSize: "12px", color: "black", float: "right" }} href="#">Tümosan<span className="ps-4"><MdNavigateNext /></span></a>
                            </li>

                        </ul>
                    </nav>
                    <div className="col-sm-7 card p-3">
                        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-indicators">
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>
                            <div className="carousel-inner">
                                <div className="carousel-item active">
                                    <img src="../img/MasseyFerguson1.jpg" className="d-block w-100" alt="..." />
                                </div>
                                <div className="carousel-item">
                                    <img src="../img/MasseyFerguson2.jpg" className="d-block w-100" alt="..." />
                                </div>
                                <div className="carousel-item">
                                    <img src="../img/MasseyFerguson1.jpg" className="d-block w-100" alt="..." />
                                </div>
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Previous</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                    <div className="col-sm-3 card  p-3">
                        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-inner">
                                <div>
                                    <p className="fs-5"><strong>Günün Fırsatları</strong></p>
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                    </div>
                                    <div class="container card-body">
                                        <h5 class="card-title"><strong>MF 240 Fren Diski</strong></h5>
                                        <h6>OEM 3578965</h6>
                                        <p class="card-text">
                                            <span style={{ color: "red" }}><strong>2,750 TL</strong></span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                        <button type="button" className="btn bg-white w-50" >
                                            <div className="row align-items-center">
                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                <button className="col-sm-8 justify-content-center rounded bg-warning text-white">
                                                    Sepete Ekle
                                                        </button>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                        <h6>OEM 3578965</h6>
                                        <p class="card-text">
                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                        <button type="button" className="btn btn-outline-secondary rounded-pill bg-white" style={{ width: "15%", height: "3rem" }}>
                                            <div className="row align-items-center">
                                                <div className="col-sm-7">
                                                    <div className="col ">
                                                        <div className="row justify-content-center">
                                                            Giriş Yap
                                            </div>
                                                        <div className="row justify-content-center" style={{ fontSize: "8px" }}>
                                                            veya Kayıt Ol
                                            </div>
                                                    </div>
                                                </div>
                                                <div className="border-start fs-4 col-sm-5 align-items-center" ><FaTractor /></div>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                                <div className="carousel-item">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                        <h6>OEM 3578965</h6>
                                        <p class="card-text">
                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                        <button type="button" className="btn bg-white w-50" >
                                            <div className="row align-items-center">
                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                    Sepete Ekle
                                                        </button>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon bg-danger" aria-hidden="true"></span>
                                <span className="visually-hidden">Previous</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span className="carousel-control-next-icon bg-danger" aria-hidden="true"></span>
                                <span className="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <h4 className="p-3 fst-italic " style={{ color: "lightgray", marginLeft: "50 !important" }} >THE MAJOR SUPPLIER OF TRACTOR SPARE PARTS IN TURKEY TÜRKİYE’NİN EN BÜYÜK TRAKTÖR YEDEK PARÇA TEDARİKÇİSİ</h4 >
            <div className="container">
                <ul className="nav">
                    <li className="me-2"><img src="../img/Agco.jpg" /></li>
                    <li className="border-end me-2"></li>
                    <li className="me-2"><img src="../img/Agco.jpg" /></li>
                    <li className="border-end me-2"></li>
                    <li className="me-2"><img src="../img/Agco.jpg" /></li>
                    <li className="border-end me-2"></li>
                    <li><img src="../img/Agco.jpg" /></li>
                </ul>
            </div>
            {/* İndirimli Ürünleri */}
            <div className="container card">
                <button className="carousel-control-prev " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon bg-danger" aria-hidden="true"></span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                    <span className="carousel-control-next-icon bg-danger" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
                <p className="row align-items-start fs-3"><strong>İndirimli Ürünler</strong></p>
                <div id="carouselExampleControls" className="carousel-inner slide" data-bs-ride="carousel">
                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row m-3">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row m-3">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Fırsat Ürünleri */}
            <div style={{ backgroundColor: "lightgrey" }}>
                <div className="container card bg-light">
                    <button className="carousel-control-prev " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon bg-danger" aria-hidden="true"></span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span className="carousel-control-next-icon  bg-danger" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>

                    <div id="carouselExampleControls" className="carousel-inner slide" data-bs-ride="carousel">
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <p className="row align-items-start fs-3"><strong>Fırsat Ürünleri</strong></p>
                                    <div class="col-lg-6">
                                        <div class="card">
                                            <img
                                                src="../img/İndirimÜrünleri.jpg"
                                                class="card-img-top"
                                                alt="..."
                                            />
                                            <div class="card-body">
                                                <h5 class="card-title fs-1 fw-bold">MF 240 Fren Diski</h5>
                                                <h6>OEM 3578965</h6>
                                                <p class="card-text fs-1 fw-bold">
                                                    <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                                <button type="button" className="btn bg-white w-50" >
                                                    <div className="row align-items-center">
                                                        <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                        <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                            Sepete Ekle
                                                        </button>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <img
                                                        src="../img/İndirimÜrünleri.jpg"
                                                        class="card-img-top"
                                                        alt="..."
                                                    />
                                                    <div class="card-body">
                                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                                        <h6>OEM 3578965</h6>
                                                        <p class="card-text">
                                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                                        <button type="button" className="btn bg-white w-50" >
                                                            <div className="row align-items-center">
                                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                                <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                                    Sepete Ekle
                                                        </button>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <img
                                                        src="../img/İndirimÜrünleri.jpg"
                                                        class="card-img-top"
                                                        alt="..."
                                                    />
                                                    <div class="card-body">
                                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                                        <h6>OEM 3578965</h6>
                                                        <p class="card-text">
                                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                                        <button type="button" className="btn bg-white w-50" >
                                                            <div className="row align-items-center">
                                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                                <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                                    Sepete Ekle
                                                        </button>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <img
                                                        src="../img/İndirimÜrünleri.jpg"
                                                        class="card-img-top"
                                                        alt="..."
                                                    />
                                                    <div class="card-body">
                                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                                        <h6>OEM 3578965</h6>
                                                        <p class="card-text">
                                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                                        <button type="button" className="btn bg-white w-50" >
                                                            <div className="row align-items-center">
                                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                                <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                                    Sepete Ekle
                                                        </button>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <img
                                                        src="../img/İndirimÜrünleri.jpg"
                                                        class="card-img-top"
                                                        alt="..."
                                                    />
                                                    <div class="card-body">
                                                        <h5 class="card-title">MF 240 Fren Diski</h5>
                                                        <h6>OEM 3578965</h6>
                                                        <p class="card-text">
                                                            <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                                        <button type="button" className="btn bg-white w-50" >
                                                            <div className="row align-items-center">
                                                                <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                                <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                                    Sepete Ekle
                                                        </button>
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            {/* Yeni Ürünleri */}
            <div className="container card">
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon bg-danger" aria-hidden="true"></span>
                </button>
                <button className="carousel-control-next " type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                    <span className="carousel-control-next-icon bg-danger" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
                <p className="row align-items-start fs-3"><strong>Fırsat Ürünleri</strong></p>
                <div id="carouselExampleControls" className="carousel-inner slide" data-bs-ride="carousel">
                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row m-3">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row m-3">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="card">
                                        <img
                                            src="../img/İndirimÜrünleri.jpg"
                                            class="card-img-top"
                                            alt="..."
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">MF 240 Fren Diski</h5>
                                            <h6>OEM 3578965</h6>
                                            <p class="card-text">
                                                <span style={{ color: "red" }}>2,750 TL</span> <span style={{ textDecoration: "line-through", color: "red" }}><span style={{ color: "black" }}>3,000 TL</span></span> </p>
                                            <button type="button" className="btn bg-white w-50" >
                                                <div className="row align-items-center">
                                                    <div className="border-end col-sm-4 align-items-center" ><FaTractor /></div>
                                                    <button className="p-2 col-sm-8 justify-content-center rounded bg-warning text-white">
                                                        Sepete Ekle
                                                        </button>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h4 className="p-3 fst-italic " style={{ color: "lightgray", marginLeft: "50 !important" }} >THE MAJOR SUPPLIER OF TRACTOR SPARE PARTS IN TURKEY TÜRKİYE’NİN EN BÜYÜK TRAKTÖR YEDEK PARÇA TEDARİKÇİSİ</h4 >
            <div className="container border-bottom border-top">
                <div id="carouselExampleIndicators" className="carousel slide ps-3" data-bs-ride="carousel">
                    <div className="carousel-inner p-2 align-items-center">
                        <div className="carousel-item active">
                            <img src="../img/Logos.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="../img/Logos.jpg" className="d-block w-100" alt="..." />
                        </div>
                        <div className="carousel-item">
                            <img src="../img/Logos.jpg" className="d-block w-100" alt="..." />
                        </div>
                    </div>
                   
                </div>
            </div>

        </div>
    )
}

export default Home;