import React from 'react'
import { FaFacebookSquare, FaInstagramSquare, FaTwitter } from 'react-icons/fa';


const Footer = () => {
    return (
        <>
            <div style={{ backgroundColor: "gray" }}>
                <nav className="navbar navbar-expand-lg text-white " >
                    <div className="container border-bottom">
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item fst-italic">EVERYTHING THAT MAKES A TRACTOR MOVE</li>
                            </ul>
                            <ul className="nav">
                                <li className="nav-item me-3"><img className="pb-1" src="../img/ENG.jpg" />ENG</li>
                                <li className="nav-item ms-2 me-3">$ 7,3705</li>
                                <li className="nav-item ms-2 me-3">€ 7,3705</li>
                                <li className="nav-item ms-2 border-end"></li>
                                <li className="nav-item ms-2 me-2"><FaInstagramSquare /></li>
                                <li className="nav-item ms-2 me-2"><FaFacebookSquare /></li>
                                <li className="nav-item ms-2 me-2"><FaTwitter /></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <nav className="navbar navbar-expand-lg text-white " >
                    <div className="container">
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav mb-2 mb-lg-0 flex-column align-items-start">
                                <li className="nav-item"><img src="../img/YedparLogo.jpg" /></li>
                                <li><p>FERHATPAŞA MAH. G13 SOK. NO 72 KAT 4</p></li>
                                <li><p>ATAŞEHİR - İSTANBUL - TÜRKİYE</p></li>
                                <li><p>+90 216 471 31 40 PBX</p></li>
                                <li><p>info@yedpar.com.tr</p></li>
                            </ul>
                            <ul className="nav ms-auto flex-column align-items-end">
                                <li><img src="../img/GüvenliAlışveriş.jpg" /></li>
                                <li><img src="../img/CreditCards.jpg" /></li>
                                <li style={{ marginTop: "7rem" }}></li>
                                <li><img src="../img/GooglePlay.jpg" /></li>
                                <li><img src="../img/AppStore.jpg" /></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <nav className="navbar navbar-expand-lg text-white " >
                    <div className="container border-top border-bottom p-2">
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <a style={{ textDecoration: "none", color: "white" }} href="www.yedpar.com.tr"><strong>www.yedpar.com.tr</strong></a>
                            </ul>
                            <ul className="nav">
                                <li className="nav-item me-3">Anasayfa</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 me-3">Ürün Arama</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 me-3">Hakkımızda</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 me-3">Yeni Ürünler</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 me-3">İndirim</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 me-3">Fırsat Ürünleri</li>
                                <li className="nav-item border-end"></li>
                                <li className="nav-item ms-3 ">İletişim</li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

        </>
    )
}
export default Footer;