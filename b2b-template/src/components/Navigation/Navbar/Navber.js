import React from 'react'
import { FaTractor, FaFacebookSquare, FaInstagramSquare, FaTwitter, FaLock } from 'react-icons/fa';
import { BsSearch } from 'react-icons/bs'

const Navbar = () => {
    return (
        <>
            <div style={{ backgroundColor: 'red' }}>
                <nav className="navbar navbar-expand-lg text-white " >
                    <div className="container border-bottom">
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item fst-italic">EVERYTHING THAT MAKES A TRACTOR MOVE</li>
                            </ul>
                            <ul className="nav m-2">
                                <li className="nav-item me-3"><img className="pb-1" src="../img/ENG.jpg" />ENG</li>
                                <li className="nav-item ms-2 me-3">$ 7,3705</li>
                                <li className="nav-item ms-2 me-3">€ 7,3705</li>
                                <li className="ms-2 border-end"></li>
                                <li className="nav-item ms-2 me-2"><FaInstagramSquare /></li>
                                <li className="nav-item ms-2 me-2"><FaFacebookSquare /></li>
                                <li className="nav-item ms-2 me-2"><FaTwitter /></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <nav className="navbar navbar-expand-lg text-white " >
                    <div className="container">
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav mb-2 mb-lg-0">
                                <li className="nav-item"><img src="../img/YedparNav.jpg" /></li>
                            </ul>
                            <form className="container-fluid">
                                <input className="form-control me-2 rounded-pill" type="search" aria-label="Search" />
                            </form>
                            <button type="button" className="btn btn-outline-secondary rounded-pill bg-white" style={{ width: "15%", height: "3rem" }}>
                                <div className="row align-items-center">
                                    <div className="col-sm-7">
                                        <div className="col ">
                                            <div className="row justify-content-center">
                                                Giriş Yap
                                            </div>
                                            <div className="row justify-content-center" style={{ fontSize: "8px" }}>
                                                veya Kayıt Ol
                                            </div>
                                        </div>
                                    </div>
                                    <div className="border-start fs-4 col-sm-5 align-items-center" ><FaLock /></div>
                                </div>
                            </button>
                            <button type="button" className="btn btn-outline-secondary rounded-pill bg-white ms-2" style={{ width: "15%", height: "3rem" }}>
                                <div className="row align-items-center">
                                    <div className="col-sm-5">
                                        <div className="col ">
                                            <div className="row justify-content-center">
                                                <span style={{
                                                    height: '30px',
                                                    width: '30px',
                                                    backgroundColor: 'red',
                                                    borderRadius: '50%',
                                                    display: 'inline-block'
                                                }} /><span style={{
                                                    fontSize: "15px",
                                                    color: "white",
                                                    position: "absolute",  
                                                    width:'auto'                                                 
                                                }}>125</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="border-start fs-4 col-sm-7 align-items-center" ><FaTractor /></div>
                                </div>
                            </button>
                        </div>
                    </div>
                </nav>
            </div>
            <nav>
                <ul className="nav justify-content-center border-bottom pt-2 pb-2">
                    <span className="fs-5 pe-2"><FaTractor /></span> <li className="nav-item me-3">Anasayfa</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 me-3">Ürün Arama</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 me-3">Hakkımızda</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 me-3">Yeni Ürünler</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 me-3">İndirim</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 me-3">Fırsat Ürünleri</li>
                    <li className="nav-item border-end"></li>
                    <li className="nav-item ms-3 ">İletişim</li>
                </ul>
            </nav>
        </>
    )
}
export default Navbar;