import React from 'react'
import './App.css';
import Navbar from './components/Navigation/Navbar/Navber';
import Home from './components/Home/Home';
import Footer from './components/Navigation/Footer/Footer'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Home />
      <Footer />
    </div>
  );
}

export default App;
